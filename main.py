"""
Entry point for ml pipeline
"""

import logging
import pandas as pd

from src.features.build_features import TfIdfTransform
from src.models.train_model import BaseLinearModel
from src.auxiliary.cli_main import ARG_PARSER


LOGGER = logging.getLogger(__name__)
SLICE_SIZE = 80


def main(args):
    """
    Describes first versions of pipeline
    """
    df_train = pd.read_csv("./data/raw/toxic_train/train.csv").iloc[:SLICE_SIZE]

    df_train = df_train.drop("id", axis=1)
    x_data = df_train["comment_text"]
    y_data = df_train[["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]]
    LOGGER.warning(y_data.shape)

    tfidf = TfIdfTransform(ngram_range=args.ngram_range)
    model = BaseLinearModel()

    x_transformed = tfidf.fit_transform(x_data)
    LOGGER.warning(f"Shape X_transformed: {x_transformed.shape} y: {y_data.shape}")

    model.fit(x_transformed, y_data)
    res = model.predict(x_transformed)

    print(res.sum())


if __name__ == "__main__":
    cli_args = ARG_PARSER.parse_args()
    main(cli_args)
