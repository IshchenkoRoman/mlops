import boto3
from botocore.exceptions import ClientError
import logging


LOGGER = logging.getLogger(__name__)


class AWSS3Provider:
    def __init__(self):
        self._s3_client = boto3.client("s3")
        self._s3_resourse = boto3.resource("s3")

    def get_file_list(self, bucket: str, folder_prefix: str = "") -> list:
        """Get list of files on s3

        Args:
            folder_prefix (str): folder on s3, where files gonna to seek

        Returns:
            list[str]: list of files on s3
        """

        bucket = self._s3_client.Bucket(bucket)
        objs = bucket.objects.filter(Prefix=folder_prefix)

        return objs

    def read(self):
        pass

    def write_file(self, file_name: str, bucket: str, object_name: str) -> bool:
        """
        This methods writes files from curent disk to s3

        Args:
            file_name (str): filepath of file
            object_name (str): filepath on s3 storage
        """

        # Upload the file
        s3_client = boto3.client("s3")
        try:
            response = s3_client.upload_file(file_name, bucket, object_name)
            LOGGER.error(response)
        except ClientError as e:
            LOGGER.error(e)
            return False
        return True

    def update(self):
        pass

    def delete(self):
        pass
