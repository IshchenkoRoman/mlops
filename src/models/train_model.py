"""
This module used for define baseline models.
"""

import logging

from datetime import datetime
from pathlib import Path
from joblib import (
    dump,
    load,
)

from sklearn.linear_model import LogisticRegression
from sklearn.multioutput import MultiOutputClassifier


LOGGER = logging.getLogger(__name__)


class BaseLinearModel:
    """
    Baseline multilabel regression
    """

    def __init__(self, **kwargs) -> None:
        self._model = MultiOutputClassifier(LogisticRegression(**kwargs))

    def fit(self, data: object, labels: object) -> None:
        """
        Train model for given data

        Args:
            data (object): 2d- array of train data
            labels (object): 2d- array of multilabeled data
        """
        self._model.fit(data, labels)

    def predict(self, data: object) -> object:
        """
        Get prediction from inputed data as labeled data

        Args:
            data (object): 2d- array of data for prediction process

        Returns:
            _type_: 2d array of answers for inputed data
        """
        return self._model.predict(data)

    def predict_proba(self, data: object) -> object:
        """
        Get prediction from inputed data as probability data

        Args:
            data (object): 2d- array of data for prediction process

        Returns:
            object: 2d array of answers for inputed data
        """
        return self._model.predict_proba(data)

    def save_model(self, path: str = "") -> None:
        """
        Save model in as binary file

        Args:
            path (str, optional): Path, where weights would be stored"".
        """
        try:
            appendix = datetime.utcnow().isoformat() + ".joblib"

            if not str:
                path = Path(f"./models/{self.__class__.__name__}")

            path.mkdir(parents=True, exist_ok=True)
            path = str(path) + appendix
            dump(self._model, path)

            LOGGER.debug(f"[{self.save_model.__name__}] Saved by path: {path}")
        except MemoryError as error:
            LOGGER.error(error)

    def load_model(self, path: str):
        """
        Load weights/ model into object

        Args:
            path (str): Path, where stored `*.joblib` file
        """
        self._model = load(path)
