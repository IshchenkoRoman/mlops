"""
This module used for defining feature engineering and data transforming steps
"""


import logging

from datetime import datetime
from pathlib import Path

from joblib import dump, load
from sklearn.feature_extraction.text import TfidfVectorizer


LOGGER = logging.getLogger(__name__)


class TfIdfTransform:
    """
    Simple base tf-idf text transformation
    """
    def __init__(self, **kwargs):
        """
        Apply tfidf transformation
        """
        self._transformer = TfidfVectorizer(**kwargs)

    def fit(self, data: object) -> None:
        """
        Calculate statistics for given data
        Args:
            data (_type_): input 2d array sparse matrix of shape n_samples, n_features
        """
        LOGGER.debug(f"[{self.trasform.__class__}] done")
        self._transformer.fit(data)

    def trasform(self, data: object) -> object:
        """
        Transform given data using tf-idf method

        Args:
            data (_type_): input 2d array sparse matrix of shape n_samples, n_features

        Returns:
            object: 2d array sparse matrix of shape n_samples, n_features
        """
        LOGGER.debug(f"[{self.trasform.__class__}] done")
        return self._transformer.transform(data)

    def fit_transform(self, data: object) -> object:
        """
        Calculate statistics for given data and transform data simultaniosly

        Args:
            data (object): input 2d array sparse matrix of shape n_samples, n_features
        Returns:
            object: 2d array sparse matrix of shape n_samples, n_features
        """
        LOGGER.debug(f"[{self.trasform.__class__}] done")
        return self._transformer.fit_transform(data)

    def save_model(self, path: str = "") -> None:
        """
        Using joblib pickle tf- idf object into binary file

        Args:
            path (str, optional): Path to folder, where model'll be stored.
        """
        try:
            appendix = datetime.utcnow().isoformat() + ".joblib"

            if not str:
                path = Path(f"./models/{self.__class__.__name__}")

            path.mkdir(parents=True, exist_ok=True)
            path = str(path) + appendix
            dump(self._transformer, path)

            LOGGER.debug(f"[{self.save_model.__name__}] Saved by path: {path}")
        except MemoryError as error:
            LOGGER.error(error)

    def load_model(self, path: str):
        """
        Using joblib upload tf-idf object

        Args:
            path (str): Path to model, where model are stored.
        """
        self._transformer = load(path)
