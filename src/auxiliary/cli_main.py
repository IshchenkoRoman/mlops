"""
Here we define cli interface for modules
"""


import argparse


def check_nargs(value: str) -> tuple:
    """
    Check if value ngram_range for tf-idf is valid

    Args:
        value (str): input values for ngram_range in TFIDf

    Raises:
        argparse.ArgumentTypeError: All values must be postive and first must be less or equal second.
        Exception: Wrong number of values

    Returns:
        tuple: return values for ngram
    """
    try:
        ngram_range_values = value.split(",")
        left_side = int(ngram_range_values[0])
        right_side = int(ngram_range_values[1])

        if left_side < 1 or right_side < 1 or left_side > right_side:
            raise argparse.ArgumentTypeError("{0} have not a positive integers "
                                             "and first number must be less or equal second".format(value))
    except IndexError:
        raise Exception("{0} must contain exact 2 values if format: {value_left,value_right}".format(value))
    return left_side, right_side


ARG_PARSER = argparse.ArgumentParser(description='MlOps example CLI')


ARG_PARSER.add_argument(
    "--ngram_range",
    type=check_nargs,
    default="1,1"
)
